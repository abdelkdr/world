import net		from "net";
import {show, init, update} from "./Map.js";


function parseMessage(msg) {
	var idx = msg.indexOf(" ");
	if (msg[0] == "#")
		return {type: "map", row: msg.split("")};
	switch(idx != -1 ? msg.substr(0, idx) : msg) {
		case 'state': return {type: "state", data: JSON.parse(msg.substr(idx + 1))};
		case 'context': return {type: "context", data: JSON.parse(msg.substr(idx + 1))};
		case 'welcome': return {type: "welcome", name: msg.split(" ")[2]};
		default: return {type: "comment", comment: msg};
	}
}



var port		= 62342;
var host		= "localhost";
var socket 	= net.createConnection(port, host, function() {

	console.log("listening");

	var buffer = "";
	socket.on("data", function (data) {

		//~ console.log("<from server>: " + data.toString().trim());
		var b = data.toString();

		if ( b[b.length - 1] != "\n" )
			buffer += b;

		else {
			notify((buffer + b).split("\n").map(x => x.trim()).filter(x => x.length));
			buffer = "";
		}

	});

});


var startSymbol;
var startPosition;
var goalPosition;
var map = [];
var endMap = false;
var moves = [];

function sendMessage(m) {
	console.log("sending message : " + m);
	socket.write(m);
	socket.write("\n");
}

function notify(messages) {

	console.log("notify");
	console.log(messages);

	var msgs = messages.map(parseMessage);

	// si le start symbol n'est pas identifié
	if ( !startSymbol ) {
		console.log(msgs);
		startSymbol = msgs[0].name;
		sendMessage("map");
		return;
	}

	// si la map n'est pas intégralement construite
	if ( !endMap ) {

		// construire la map ligne par ligne
		for(var i = 0; i < msgs.length; i++) {
			if ( !endMap && msgs[i].type == "map" ) {
				var j = msgs[i].row.indexOf(startSymbol);
				var k = msgs[i].row.indexOf("G");
				if (  j != -1 )
					startPosition = [map.length, j];
				if ( k != -1 )
					goalPosition = [map.length, k];
				map.push(msgs[i].row);
			}
			else endMap = true;
		}

		// si la map est construite
		if ( endMap ) {

			// calculer le chemin
			console.log("compute path");
			moves = decision(map, startPosition, goalPosition);

			console.log("path computed !");
			console.log(moves);
			// envoyer le premier mouvement

			console.log("send first move : " + moves[0]);
			sendMessage("move " + moves.shift());
		}

		return;
	}

	while(msgs.length && msgs[0].type != "context")
		msgs.shift();

	if (!msgs.length)
		return;

	console.log("send next move : " + moves[0]);
	// ignorer les messages de context du serveur, mais quand meme envoyer les mouvements restants
	if ( moves.length )
		sendMessage("move " + moves.shift());

}

function decision(map, source, target) {

	console.log(map);
	console.log(source);
	console.log(target);
	return dijkstra(map, source, target);
	return astar(map, source, target);

}

function dijkstra(map, [i, j]) {

	var current, extendedPath, cell = `${i}-${j}`;
	var distances = {[cell]: 0};
	var queue = [{position: [i, j], moves: [], distance: 0}];

	while ( map[queue[0].position[0]][queue[0].position[1]] != "G" ) {

		current = queue.shift();
		extendedPath = getVoisins(map, current.position).map(v => ({
			position: v.position,
			moves: current.moves.concat([v.move]),
			distance: current.distance + 1,
		}));

		for(var p of extendedPath) {
			var {position: [i, j], distance} = p;
			cell = `${i}-${j}`;

			if ( !(cell in distances) || distances[cell] > distance ) {
				// mise à jour du plus court chemin de la source à la cellule courante
				distances[cell] = distance;
				// on remet les nouveaux chemins dans la liste des chemins à explorer
				insertQueue(queue, p);
			}

		}

	}

	return queue[0].moves;

}




function astar(map, source, target) {

}


function insertQueue(queue, path) {
	var insertPos = 0;
	for(var i = 0; i < queue.length; i++)
		if (path.distance < queue[0].distance)
			break;
		else insertPos++;

	queue.splice(insertPos, 0, path);
}


function getVoisins(map, [i, j]) {
	var lst = [];

	if ( map[i][j - 1] != '#' )
		lst.push({position: [i, j - 1], move: "west"});

	if ( map[i][j + 1] != '#' )
		lst.push({position: [i, j + 1], move: "east"});

	if ( map[i - 1][j] != '#' )
		lst.push({position: [i - 1, j], move: "north"});

	if ( map[i + 1][j] != '#' )
		lst.push({position: [i + 1, j], move: "south"});

	return lst;
}


